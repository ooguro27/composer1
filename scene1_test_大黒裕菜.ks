*start

[cm]
[clearfix]
[chara_config pos_mode=false]

[mask time=0]


[position layer="message0" left=20 top=450 width=920 height=150 page=fore visible=true]

[position layer=message0 page=fore margint="35" marginl="50" marginr="70" marginb="60"]

[ptext name="chara_name_area" layer="message0" color="white" size=22 x=50 y=460]

[chara_config ptext="chara_name_area"]

;ひとみ
[chara_new name="hito" storage="chara/hito/hito01-1.png" jname="ひとみ"]
;あおい
[chara_new name="aoi" storage="chara/aoi/normal.png" jname="あおい"]
;ひとみ表情
[chara_face name="hito" face="warau" storage="chara/hito/hito01-1.png" jname="ひとみ"]
[chara_face name="hito" face="human" storage="chara/hito/hito02-200x300.png" jname="ひとみ"]
[chara_face name="hito" face="oko" storage="chara/hito/hito03.png" jname="ひとみ"]
[chara_face name="hito" face="doki" storage="chara/hito/hito04.png" jname="ひとみ"]
[chara_face name="hito" face="niko" storage="chara/hito/hito05.png" jname="ひとみ"]
[chara_face name="hito" face="miken" storage="chara/hito/hito06.png" jname="ひとみ"]
;あおい表情
[chara_face name="aoi" face="oko" storage="chara/aoi/angry.png" jname="あおい"]
[chara_face name="aoi" face="niko" storage="chara/aoi/happy.png" jname="あおい"]
[chara_face name="aoi" face="doki" storage="chara/aoi/doki.png" jname="あおい"]
[chara_face name="aoi" face="no" storage="chara/aoi/normal.png" jname="あおい"]
[chara_face name="aoi" face="sad" storage="chara/aoi/sad.png" jname="あおい"]
@showmenubutton

;背景
[bg storage="gaikan_hiru.jpg" time=0 wait=true]
[chara_mod name="hito" face="niko"]
[chara_show name="hito" left="100" top="50"]
[chara_show name="aoi" left="650" top="200" width="200" height="300"]
[playbgm storage="comedy.ogg"]
[mask_off time=100]
[wait time=200]

@layopt layer=message0 visible=true

#ひとみ
……………………[p]
@layopt layer=message0 visible=false
[wait time=200]
[chara_mod name="hito" face="warau"]
[wait time=500]
[mask]
[chara_hide name="aoi" time="0"]
[chara_show name="aoi" left="500" top="50" width="400" height="600"]
[chara_mod name="hito" face="niko"]

[mask_off]
[anim name=hito left="+=200" time=500]
[wait time=100]
@layopt layer=message0 visible=true
#ひとみ
……つんつん[p]
[anim name=hito left="-=150" time=100]
[chara_mod name="aoi" face="doki"]
[chara_config talk_anim="up" talk_anim_time="300"]

#あおい
ぎゃ～！！[p]

[chara_mod name="aoi" face="oko"]
[chara_config talk_anim="none"]

#あおい
何するのよ！！！　[chara_mod name="aoi" face="sad"]足しびれてたのに～！！！！[p]

[chara_mod name="hito" face="warau"]

#ひとみ
あはは、ごめんごめん[p]
@layopt layer=message0 visible=false
[mask]
[stopbgm]
[chara_hide name="aoi" time="0"]
[chara_hide name="hito" time="0"]
[chara_mod name="hito" face="niko" time="0"]
[chara_show name="hito" left="280" top="50" width="400" height="600" time="0"]
[playbgm storage="normal.ogg"]
[mask_off]
@layopt layer=message0 visible=true
#ひとみ
ねえねえ、これは何？[p]

[chara_hide name="hito" time="0"]
[chara_mod name="aoi"  face="niko"]
[chara_show name="aoi" left="280" top="50" width="400" height="600" time="0"]

#あおい
ああ、それね。焼き芋よ[p]

[chara_hide name="aoi" time="0"]
[chara_mod name="hito" face="warau"]
[chara_show name="hito" left="280" top="50" width="400" height="600" time="0"]

#ひとみ
へ～、おいしそうだね！[p]

[chara_hide name="hito" time="0"]
[chara_mod name="aoi" face="no"]
[chara_show name="aoi" left="280" top="50" width="400" height="600" time="0"]

#あおい
今、焼くから待ってなさい[p]
@layopt layer=message0 visible=false
[mask]
[chara_hide name="aoi"]
[chara_show name="hito" left="100" top="50" width="400" height="600"]
[chara_mod name="hito" face="doki"]
[chara_show name="aoi" left="450" top="50" width="400" height="600"]
[mask_off]
[anim name=hito left="-=50" time=300]
[anim name=hito left="+=50" time=300]
[anim name=hito left="+=50" time=300]
[anim name=hito left="-=50" time=300]
[anim name=hito left="-=50" time=300]
[anim name=hito left="+=50" time=300]
[wait time=500]
@layopt layer=message0 visible=true
#ひとみ
……[p]

[chara_mod name="aoi" face="sad"]
#あおい
落ち着かないみたいだけど、どうしたの？[p]

[chara_mod name="hito" face="human"]
#ひとみ
そろそろ焼けたかなって……[p]

[chara_mod name="aoi" face="no"]
#あおい
そうね、もう少し時間が掛かると思うわ[p]

[chara_mod name="hito" face="miken"]
#ひとみ
分かった……[p]
@layopt layer=message0 visible=false
[wait time=200]
[anim name=hito left="-=500" time=3000 effect="easeInSine"]
[wait time=2000]
[mask]
[chara_config talk_anim="none"]
[chara_hide name="aoi"]
[chara_hide name="hito"]
[chara_mod name="aoi" face="niko"]
[chara_show name="aoi" left="280" top="50"]
[bg storage="gaikan_yu.jpg" time=1500 wait=true]
[mask_off]
@layopt layer=message0 visible=true
#あおい
うん、いい感じ！　はい、どうぞ[p]
@layopt layer=message0 visible=false
[chara_hide name="aoi" time=0]
[chara_show name="hito" left="280" top="50" time=0]
[anim name=hito top="-=50" time=300]
[anim name=hito top="+=50" time=300]
[wait time=500]
[anim name=hito top="-=50" time=300]
[anim name=hito top="+=50" time=300]
[wait time=1200]
[chara_hide name="hito" time=0]
[chara_mod name="hito" face="niko"]
[chara_show name="hito" left="100" top="50" time=0]
[chara_show name="aoi" left="450" top="50" time=0]
@layopt layer=message0 visible=true
#あおい
それじゃあ[p]

[chara_mod name="hito" face="warau" time=0]
[chara_mod name="aoi" face="niko" time=0]
#ひとみ・あおい
「「いっただきまーす！」」[p]
@layopt layer=message0 visible=false
[stopbgm]
[wait time=1000]
[chara_mod name="hito" face="doki"]
[wait time=300]
[chara_hide name="aoi" time=0]
[chara_hide name="hito" time=0]
[chara_mod name="hito" face="miken"]
[chara_show name="hito" left="280" top="50" time=0]
[quake count=5 time=300 hmax=20]
[wait time=500]
[playbgm storage="dozikkomarch.ogg"]
[anim name=hito left="+=100" time=200]
[anim name=hito left="-=100" time=200]
[anim name=hito left="-=100" time=200]
[anim name=hito left="+=100" time=200]
[wait time=500]
[chara_config talk_anim="up" talk_anim_value="200"]
[wait time=500]
@layopt layer=message0 visible=true
#ひとみ
……う、うぅ～！！！！！[p]
[wait time=200]
@layopt layer=message0 visible=false
[wait time=500]
[chara_hide name="hito" time=0]
[chara_mod name="aoi" face="doki"]
[chara_show name="aoi" left="280" top="50" time=0]
[chara_config talk_anim="none"]
[wait time=500]
@layopt layer=message0 visible=true
#あおい
……熱いって！？　
[wait time=500]
[anim name=aoi left="+=200" time=200]
[anim name=aoi left="-=200" time=200]
[anim name=aoi left="-=200" time=200]
[anim name=aoi left="+=200" time=200]
大変、お水お水！[p]
[wait time=200]
@layopt layer=message0 visible=false
[wait time=200]
[anim name=aoi left="-=700" time=500 effect="easeInSine"]
[wait time=500]
[mask]
[stopbgm ]
@hidemenubutton
[chara_hide_all time=0]
[freeimage layer="base"]
[mask_off]
[jump storage=title.ks]