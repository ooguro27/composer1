*start

[cm]
[clearfix]
[chara_config pos_mode=false]

[mask time=0]


[position layer="message0" left=20 top=450 width=920 height=150 page=fore visible=true]

[position layer=message0 page=fore margint="35" marginl="50" marginr="70" marginb="60"]

[ptext name="chara_name_area" layer="message0" color="white" size=22 x=50 y=460]

[chara_config ptext="chara_name_area"]
;あかね
[chara_new name="aka" storage="chara/akane/normal.png" jname="あかね"]
;あおい
[chara_new name="aoi" storage="chara/aoi/normal.png" jname="あおい"]
;あかね表情
[chara_face name="aka" face="oko" storage="chara/akane/angry.png" jname="あかね"]
[chara_face name="aka" face="sad" storage="chara/akane/sad.png" jname="あかね"]
[chara_face name="aka" face="back" storage="chara/akane/back.png" jname="あかね"]
[chara_face name="aka" face="doki" storage="chara/akane/doki.png" jname="あかね"]
[chara_face name="aka" face="no" storage="chara/akane/normal.png" jname="あかね"]
[chara_face name="aka" face="niko" storage="chara/akane/happy.png" jname="あかね"]
[chara_face name="aka" face="niko2" storage="chara/akane/happy2.png" jname="あかね"]
;あおい表情
[chara_face name="aoi" face="oko" storage="chara/aoi/angry.png" jname="あおい"]
[chara_face name="aoi" face="niko" storage="chara/aoi/happy.png" jname="あおい"]
[chara_face name="aoi" face="doki" storage="chara/aoi/doki.png" jname="あおい"]
[chara_face name="aoi" face="no" storage="chara/aoi/normal.png" jname="あおい"]
[chara_face name="aoi" face="sad" storage="chara/aoi/sad.png" jname="あおい"]
@showmenubutton
[keyframe name="hasiru"]
[frame p=10% y=-75]
[frame p=20% y=-150 scale=0.9]
[frame p=30% y=-225 scale=0.8]
[frame p=40% y=-300 scale=0.7]
[frame p=50% y=-375 scale=0.6]
[frame p=60% y=-450 scale=0.5]
[frame p=70% y=-525 scale=0.4]
[frame p=80% y=-600 scale=0.3]
[frame p=90% y=-675 scale=0.2]
[frame p=100% y=-750 scale=0.1]
[endkeyframe]

;背景
[bg storage="suizokukan2.jpg" time=0 wait=true]
[mask_off time=200]
[wait time=1000]
[bg storage="sand.jpg" time=1500 wait=true method="fadeInUp"]
[playbgm storage="comedy.ogg"]
[chara_show name="aoi" left="100" top="50" time=0]
[chara_show name="aka" left="450" top="50" time=0]
[wait time=200]
@layopt layer=message0 visible=true
[chara_mod name="aka" face="sad"]
#あかね
あおいちゃん、海まで来て制服って、ないと思うんだ……[p]

[chara_mod name="aoi" face="oko"]
#あおい
なに言ってるの？学生はどんな時でも制服を着用するものよ！[p]

[chara_mod name="aoi" face="niko"]
#あおい
あかねも制服に着替えましょう！[p]

[chara_mod name="aka" face="oko"]
#あかね
やだよ……、[wait time=500][chara_mod name="aka" face="no"]私は海で泳いてくる！[p]
@layopt layer=message0 visible=false

[chara_mod name="aoi" face="sad"]

[wait time=800]

[chara_hide_all time=0]
[chara_mod name="aka" face="niko2"]
[chara_show name="aka" left="280" top="0"]
@layopt layer=message0 visible=true
#あかね
じゃあね、あおいちゃん！[p]
@layopt layer=message0 visible=false
[wait time=200]
[chara_mod  name="aka" face="back"]
[playse storage=landing.ogg loop=true]
[kanim name="aka" keyframe="hasiru" time=1500]
[chara_hide name="aka"]
[stopse ]
[mask]
[stopbgm ]
[chara_mod name="aoi" face="oko"]
[chara_show name="aoi" left="280" top="50"]
[playbgm storage="normal.ogg"]
[bg storage="living.jpg" time=0 wait=true]
[mask_off]
@layopt layer=message0 visible=true
#あおい
あかね、起きて！　[wait time=300]こんなところで寝たら風邪ひくよ！[p]

[chara_hide name="aoi" time=0]
[chara_mod name="aka" face="sad" time=0]
[chara_show name="aka" left="280" top="200" time=0]
[chara_move name="aka" anim="true" top="50" time=1000 effect="easeInQuad"]
#あかね
……うぅ……寒い……
[wait time=800]
[chara_mod name="aka" face="doki"][anim name=aka top="-=50" time=100][anim name=aka top="+=50" time=100]
――っくしゅん！[p]

[chara_hide name="aka" time=0]
[chara_config talk_anim="none"]
[chara_mod name="aoi" face="sad" time=0]
[chara_show name="aoi" left="280" top="50" time=0]
#あおい
はぁ……、[wait time=500]風邪ひいたんじゃない？[p]

[chara_hide name="aoi" time=0]
[chara_mod name="aoi" face="no" time=0]
[chara_mod name="aka" face="sad" time=0]
[chara_show name="aoi" left="100" top="50" time=0]
[chara_show name="aka" left="450" top="50" time=0]

#あおい
ちょっとこっちに来て、おでこ出してみて[p]

[chara_mod name="aka" face="doki"]
#あかね
え？[wait time=500]……あれ……[wait time=300]私の海は？[p]

[chara_mod name="aoi" face="oko"]
#あおい
なに訳のわからないこと言ってるの！[chara_mod name="aka" face="sad"][wait time=300]いいからおでこを出す！[p]

[chara_move name="aoi" anim="true" left="200" time=300 effect="easeInQuad"]
#あかね
あ……[p]

[chara_mod name="aka" face="doki"]
#あかね
……えっと、それは恥ずかしいよ～[p]

#あおい
いいから！　
[wait time=500][chara_mod name="aka" face="sad"][chara_move name="aka" anim="true" left="400" time=1000 effect="easeInQuad"][wait time=500][chara_mod name="aoi" face="sad"]
うーん、ちょっと熱っぽい気もするけど……[p]

[chara_mod name="aoi" face="oko"]
#あおい
もう少ししっかりおでこかしなさいよ[p]
[chara_move name="aoi" anim="true" left="280" time=200 effect="easeInQuad"][chara_mod name="aka" face="doki"]
[wait time=500]
[chara_mod name="aoi" face="sad"]
#あおい
うーん、やっぱり少しありそうな感じね……[p]
[wait time=500]
[chara_move name="aka" anim="true" left="500" time=100 effect="easeInQuad"]
[chara_mod name="aka" face="oko"]
#あかね
だ、大丈夫だから！　[chara_move name="aka" anim="true" left="600" time=100 effect="easeInQuad"]そろそろ、お風呂入って寝るから！[p]
[chara_mod name="aka" face="no"]
#あかね
あおいちゃんも風邪ひかないように気を付けてね！[p]
[chara_mod name="aoi" face="no"]
@layopt layer=message0 visible=false
[chara_mod name="aka" face="back"]
[chara_move name="aka" anim="true" left="1000" time=500 effect="easeInQuad"]
[mask time=500]
@reset_camera
[stopbgm ]
@hidemenubutton
[chara_hide_all time=0]
[stopbgm ]
[freeimage layer="base"]
[mask_off]
[jump storage=title.ks]
