
;■条件
;・使用キャラクターは自由
;・背景は何を使用してもOK

;■スクリプトを組む上でのポイント
;・急に話が飛んでいる場合に、場所などをユーザーが理解できるようにスクリプトが組めるか
;・キャラ性を理解できているか

*start

[cm]
[clearfix]
[chara_config pos_mode=false]

[position layer="message0" left=20 top=400 width=920 height=200 page=fore visible=true]

[position layer=message0 page=fore margint="45" marginl="50" marginr="70" marginb="60"]

[ptext name="chara_name_area" layer="message0" color="white" size=24 x=50 y=410]

[chara_config ptext="chara_name_area"]

;————————以下よりスクリプトを組んでください————————

@layopt layer=message0 visible=true

;キャラ１定義
[chara_new name="akane" storage="chara/akane/normal.png" jname="あかね"]
;キャラ２定義
[chara_new name="yamato" storage="chara/yamato/normal.png" jname="やまと"]
;キャラ１表情
[chara_face name="akane" face="happy" storage="chara/akane/happy.png" jname="あかね"]
[chara_face name="akane" face="doki" storage="chara/akane/doki.png" jname="あかね"]
[chara_face name="akane" face="sad" storage="chara/akane/sad.png" jname="あかね"]
[chara_face name="akane" face="angry" storage="chara/akane/angry.png" jname="あかね"]
;キャラ２表情
[chara_face name="yamato" face="angry" storage="chara/yamato/angry.png" jname="やまと"]
[chara_face name="yamato" face="tohoho" storage="chara/yamato/tohoho.png" jname="やまと"]
[chara_face name="yamato" face="sad" storage="chara/yamato/sad.png" jname="やまと"]
[playbgm storage="nomal.ogg"]
;背景
[bg storage="nc71313.jpg" time="1000"]
;メニューボタン
@showmenubutton
[chara_show name="akane" left=100 top=100]
#あかね
やまと！　早く早くー！[r]
[chara_mod name="akane"  face="angry"]
そんなのんびりしてると、遅刻しちゃうぞ！[p]
[chara_mod name="yamato"  face="angry"]
[chara_show name="yamato" left=500 top=50]
#やまと
……分かってるって！ この坂道きついんだよ！[p]
[cm]

@layopt layer=message0 visible=false

[anim name=akane left="-=500" time=2000 effect="easeInSine"]
[anim name=yamato left="-=1000" time=2000 effect="easeInSine"]
[mask time=500]
[wa]
[chara_hide name="akane"]
[chara_hide name="yamato"]
[bg storage="room.jpg" time="1"]
[chara_mod name="akane"  face="happy"]
[chara_show name="akane" left=280 top=100 layer=1]
@layopt layer=message0 visible=true
[mask_off time=500]

#あかね
到着！[r]
みんな、おっはよー！[p]
[anim name=akane left="-=180" time=500 effect="easeInSine"]

[chara_mod name="yamato"  face="sad"]
[wait time=500]
[chara_show name="yamato" left=500 top=50 layer=1]
#やまと
はぁはぁ……[r]
[chara_mod name="yamato"  face="angry"]
置いてくんじゃねえよ……この体力馬鹿が！[p]
[chara_mod name="akane"  face="angry"]
#あかね
ほら！ 授業始まっちゃうよー[r]
席についてー[p]
[chara_mod name="yamato"  face="tohoho"]
#やまと
はあ……、わかったよ[p]
[mask]
[chara_hide_all]
[chara_hide_all layer=1]
[freeimage layer="base"]
#
@layopt layer=message0 visible=false
[mask_off]
[jump first.ks]


 