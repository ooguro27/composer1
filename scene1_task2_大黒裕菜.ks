
;■条件
;・使用キャラクターは自由
;・背景は何を使用してもOK

;■スクリプトを組む上でのポイント
;・抱き着く際の演出に違和感がないように
;・普通に組むと盛り上がりに欠けるので、どこかしらに演出を

*start

[cm]
[clearfix]
[chara_config pos_mode=false]

[position layer="message0" left=20 top=400 width=920 height=200 page=fore visible=true]

[position layer=message0 page=fore margint="45" marginl="50" marginr="70" marginb="60"]

[ptext name="chara_name_area" layer="message0" color="white" size=24 x=50 y=410]

[chara_config ptext="chara_name_area"]

;————————以下よりスクリプトを組んでください————————

@layopt layer=message0 visible=true

;キャラ１定義
[chara_new name="miku" storage="chara/aoi/normal.png" jname="みく"]
;キャラ２定義
[chara_new name="hikari" storage="chara/hyo/hyo1.png" jname="ひかり"]
;キャラ１表情
[chara_face name="miku" face="happy" storage="chara/aoi/happy.png" jname="みく"]
[chara_face name="miku" face="doki" storage="chara/aoi/doki.png" jname="みく"]
[chara_face name="miku" face="sad" storage="chara/aoi/sad.png" jname="みく"]
[chara_face name="miku" face="angry" storage="chara/aoi/angry.png" jname="みく"]

;キャラ２表情
[chara_face name="hikari" face="2" storage="chara/hyo/hyo2.png" jname="ひかり"]
[chara_face name="hikari" face="3" storage="chara/hyo/hyo3.png" jname="ひかり"]
[chara_face name="hikari" face="4" storage="chara/hyo/hyo4.png" jname="ひかり"]
[chara_face name="hikari" face="5" storage="chara/hyo/hyo5.png" jname="ひかり"]
[chara_face name="hikari" face="6" storage="chara/hyo/hyo6.png" jname="ひかり"]
[chara_face name="hikari" face="7" storage="chara/hyo/hyo7.png" jname="ひかり"]
[chara_face name="hikari" face="8" storage="chara/hyo/hyo8.png" jname="ひかり"]
[playbgm storage="nomal.ogg"]
;背景
[bg storage="gaikan_yu.jpg" time="1000"]
;メニューボタン
@showmenubutton
[chara_mod name="miku"  face="happy"]
[chara_show name="miku" left=280 top=100]
#みく
ひかりちゃーん！[p]
@layopt layer=message0 visible=false
[anim name=miku top="+=200" time=1000 effect="easeOutCirc"]
[mask]
[chara_hide_all]
[playse storage="人にぶつかりました.ogg"]
@layopt layer=message0 visible=true
[chara_mod name="hikari"  face="4"]
[chara_show name="hikari" left=280 top=100]
[chara_mod name="miku"  face="happy"]
[chara_show name="miku" left=200 top=100]
[mask_off]
#ひかり
みく！？[r]
急に抱き着かないでよ！[p]
#みく
えー！　いいじゃん[r]
ところで、ひかりちゃんは何やってるの？[p]
[chara_mod name="hikari"  face="2"]
#ひかり
はぁ……、珍しい鳥がいたから見てたのよ[p]
[chara_mod name="miku"  face="doki"]
#みく
え！　どこどこ！？[p]
@layopt layer=message0 visible=false
[chara_hide name="hikari"]
[anim name=miku left="-=300" time=1000 effect="easeOutCirc"]
[wait time=2000]
[anim name=miku left="+=800" time=1000 effect="easeOutCirc"]
[chara_hide name="miku"]
[chara_show name="hikari" left=280 top=100]
[chara_show name="miku" left=200 top=100]
[chara_mod name="miku"  face="happy"]
@layopt layer=message0 visible=true
#みく
あ！　そういえば、これから新しく出来たカフェ行くんだけど、一緒に行かない？[p]
[chara_mod name="hikari"  face="7"]
#ひかり
もう少し鳥を見たいから遠慮しとくわ[p]
[chara_mod name="miku"  face="sad"]
#みく
えー！　行こうよー[p]
@layopt layer=message0 visible=false
[anim name=miku top="+=200" time=1000 effect="easeOutCirc"]
[anim name=miku top="-=100" time=1000 effect="easeOutCirc"]
[wait time=2000]
@layopt layer=message0 visible=true
[chara_mod name="hikari"  face="5"]
#ひかり
ちょっと人前で恥ずかしいから辞めなさい！[r]
と言うより……いつまで抱き着いてるのよ！[p]
[mask]
[chara_hide_all]
[chara_hide_all layer=1]
[freeimage layer="base"]
#
@layopt layer=message0 visible=false
[mask_off]
[jump first.ks]

 