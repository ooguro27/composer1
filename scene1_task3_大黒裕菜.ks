
;■条件
;・キャラクターはあかね、やまとを使用
;・やまとの表情はnormal、angry使用不可
;・背景は何を使用してもOK

;■スクリプトを組む上でのポイント
;・真剣な表情がない中でどう演出するか
;・後半の靴の話でどのような演出をするか

*start

[cm]
[clearfix]
[chara_config pos_mode=false]

[position layer="message0" left=20 top=400 width=920 height=200 page=fore visible=true]

[position layer=message0 page=fore margint="45" marginl="50" marginr="70" marginb="60"]

[ptext name="chara_name_area" layer="message0" color="white" size=24 x=50 y=410]

[chara_config ptext="chara_name_area"]

;————————以下よりスクリプトを組んでください————————
;キャラ１定義
[chara_new name="akane" storage="chara/akane/normal.png" jname="あかね"]
;キャラ２定義
[chara_new name="yamato" storage="chara/yamato/normal.png" jname="やまと"]
;キャラ１表情
[chara_face name="akane" face="happy" storage="chara/akane/happy.png" jname="あかね"]
[chara_face name="akane" face="doki" storage="chara/akane/doki.png" jname="あかね"]
[chara_face name="akane" face="sad" storage="chara/akane/sad.png" jname="あかね"]
[chara_face name="akane" face="angry" storage="chara/akane/angry.png" jname="あかね"]
[chara_face name="akane" face="normal" storage="chara/akane/normal.png" jname="あかね"]
[chara_face name="akane" face="normal2" storage="chara/akane/normal2.png" jname="あかね"]
;キャラ２表情
[chara_face name="yamato" face="tohoho" storage="chara/yamato/tohoho.png" jname="やまと"]
[chara_face name="yamato" face="sad" storage="chara/yamato/sad.png" jname="やまと"]
[chara_face name="yamato" face="happy" storage="chara/yamato/happy.png" jname="やまと"]
[chara_face name="yamato" face="happy2" storage="chara/yamato/happy2.png" jname="やまと"]
[playbgm storage="nomal.ogg"]
;背景
[bg storage="okujou.jpg" time="1000"]
;メニューボタン
@showmenubutton
[chara_mod name="yamato"  face="tohoho"]
[chara_show name="yamato" left=100 top=50]
[chara_mod name="akane"  face="doki"]
[chara_show name="akane" left=450 top=100]
@layopt layer=message0 visible=true
#あかね
やまとがすごく真剣な顔をしてる……。何あったの！？[p]
#やまと
SNSにどの写真載せようか悩んでるんだよ……[p]
[chara_mod name="akane"  face="happy"]
#あかね
楽しそうなの載せたら良いと思うな！[p]
[chara_mod name="yamato"  face="happy"]
#やまと
じゃあ一緒に撮ろうぜ！[p]
[chara_mod name="akane"  face="sad"]
#あかね
でも、ただ載せるんじゃ面白くないよ？[p]
[chara_mod name="yamato"  face="tohoho"]
#やまと
じゃあ……[p]
[chara_mod name="akane"  face="happy"]
#あかね
あ！　やまと新しい靴履いてたよね！　私も買ったばかりだから、靴履いて写真撮ってみない？[p]
@layopt layer=message0 visible=false
[mask]
[chara_hide name="akane"]
[chara_hide name="yamato"]
[chara_mod name="yamato"  face="happy2"]
[chara_show name="yamato" left=150 top=50]
[mask_off]
@layopt layer=message0 visible=true
#やまと
じゃあ、撮るぜ！[p]
@layopt layer=message0 visible=false
[chara_hide name="yamato"]
[chara_mod name="akane"  face="normal2"]
[chara_show name="akane" left=250 top=100]
@layopt layer=message0 visible=true
#あかね
ちょっと待て！　やっぱり、もう少し靴を近くした方がよくない？[p]
@layopt layer=message0 visible=false
[chara_hide name="akane"]
[bg storage="aosora.jpg" time="1"]
@layopt layer=message0 visible=true
#やまと
こんな感じか？[r]
なら……はい、チーズ[p]
@layopt layer=message0 visible=false
[bg storage="okujou.jpg" time="500"]
[chara_mod name="akane"  face="happy"]
[chara_show name="akane" left=100 top=100]
@layopt layer=message0 visible=true
#あかね
いい感じの写真撮れた？[p]
[chara_mod name="yamato"  face="happy"]
[chara_show name="yamato" left=450 top=50]
#やまと
これならSNSに載せられそうだな！[p]
[mask]
[chara_hide_all]
[chara_hide_all layer=1]
[freeimage layer="base"]
#
@layopt layer=message0 visible=false
[mask_off]
[jump first.ks]
 