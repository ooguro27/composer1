
;■条件
;・キャラクターはあかね、やまとを使用
;・背景は何を使用してもOK

;■スクリプトを組む上でのポイント
;・鏡を見ている際にどのような演出をするか

*start

[cm]
[clearfix]
[chara_config pos_mode=false]

[position layer="message0" left=20 top=400 width=920 height=200 page=fore visible=true]

[position layer=message0 page=fore margint="45" marginl="50" marginr="70" marginb="60"]

[ptext name="chara_name_area" layer="message0" color="white" size=24 x=50 y=410]

[chara_config ptext="chara_name_area"]

@layopt layer=message0 visible=true

;————————以下よりスクリプトを組んでください————————

@layopt layer=message0 visible=true

;キャラ１定義
[chara_new name="akane" storage="chara/akane/normal.png" jname="あかね"]
;キャラ２定義
[chara_new name="yamato" storage="chara/yamato/normal.png" jname="やまと"]
;キャラ１表情
[chara_face name="akane" face="happy" storage="chara/akane/happy.png" jname="あかね"]
[chara_face name="akane" face="doki" storage="chara/akane/doki.png" jname="あかね"]
[chara_face name="akane" face="back" storage="chara/akane/back.png" jname="あかね"]
[chara_face name="akane" face="normal" storage="chara/akane/normal.png" jname="あかね"]
;キャラ２表情
[chara_face name="yamato" face="tohoho" storage="chara/yamato/tohoho.png" jname="やまと"]
[chara_face name="yamato" face="happy" storage="chara/yamato/happy.png" jname="やまと"]
[playbgm storage="nomal.ogg"]

;背景
[bg storage="siro.jpg" time="1"]
;メニューボタン
@showmenubutton
[chara_mod name="akane"  face="back"]
[chara_show name="akane" left=200 top=100]
#あかね
鏡よ、鏡よ、鏡さん[r]
[chara_mod name="akane"  face="happy"]
世界で一番美しいのはだーれ？[p]
@layopt layer=message0 visible=false
[chara_hide name="akane"]
[bg storage="gym.jpg" time="500"]
[chara_mod name="yamato"  face="tohoho"]
[chara_show name="yamato" left=200 top=50]
@layopt layer=message0 visible=true
#やまと
……おまえ、いったい何やってるんだ？？[p]
@layopt layer=message0 visible=false
[chara_hide name="yamato"]
[chara_mod name="akane"  face="doki"]
[chara_show name="akane" left=200 top=100]
@layopt layer=message0 visible=true
#あかね
や、やまと！？[p]
@layopt layer=message0 visible=false
[chara_hide name="akane"]
[chara_mod name="yamato"  face="tohoho"]
[chara_show name="yamato" left=200 top=50]
@layopt layer=message0 visible=true
#やまと
あまりにも真剣にやってるから、ちょっと声かけ辛くてな……[p]
@layopt layer=message0 visible=false
[chara_hide name="yamato"]
[chara_mod name="akane"  face="doki"]
[chara_show name="akane" left=200 top=100]
@layopt layer=message0 visible=true
#あかね
あはは……[p]
[chara_mod name="akane"  face="normal"]
#あかね
実は、演劇の練習をしていたんだ！[p]
@layopt layer=message0 visible=false
[chara_hide name="akane"][chara_mod name="akane"  face="happy"][chara_show name="akane" left=100 top=100]
@layopt layer=message0 visible=true
#あかね
あ！　	やまとも練習手伝ってよ！　これ台本ね！[p]
[chara_mod name="yamato"  face="tohoho"]
[chara_show name="yamato" left=450 top=50]
#やまと
まだやるって言ってないんだが……[p]
[chara_mod name="akane"  face="normal"]
#あかね
まあまあ、そんなこと言わずに……[r]
[chara_mod name="akane"  face="happy"]
あとでアイス御馳走するから！[p]
[chara_mod name="yamato"  face="happy"]
#やまと
やらせていただきます！[p]
[chara_mod name="akane"  face="happy"]
#あかね
本当に！　やった！[p]
[mask]
[chara_hide name="akane"][chara_hide name="yamato"]
[chara_show name="akane" left=200 top=100]
[mask_off]
#あかね
よーし！　練習再開だ！[p]
[mask]
[chara_hide_all]
[chara_hide_all layer=1]
[freeimage layer="base"]
#
@layopt layer=message0 visible=false
[mask_off]
[jump first.ks]
